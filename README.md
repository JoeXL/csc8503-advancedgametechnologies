# Advanced Game Technologies Project
## Project Overview
### Objective
The objective of the project was to create a golf game that makes use of advanced physics techniques, state machines, path finding and networking.

### Video Demo

https://www.youtube.com/watch?v=nVMjyg-Cdaw&feature=youtu.be

### Controls on main menu:
* 1 - Start the game with no networking
* 2 - Start the game as a host (if a server already exists you'll join as a client)
* 3 - Start the game as a client

### Controls in game:
* Q - Toggle mouse mode (Mode 1 allows for camera movement. Mode 2 allows for clicking on the ball)
* R - Reset the current level
* T - Reset the camera position (when not in ball camera)
* G - Toggle gravity
* B - Toggle ball camera
* I - Toggle drawing the A* path (only on robot level)
* O - Toggle drawing the navigation grid (only on robot level)
* P - Toggle drawing the quad tree
* Left - Go to the previous level
* Right - Go to the next level

To hit the ball: Click on the ball and then drag

## Build Information
The solution file is in the root directory

It is currently only setup to build for win32

The startup project should be set to GolfGame

Build directories are in the **Win32** folder then either **Release** or **Debug**






