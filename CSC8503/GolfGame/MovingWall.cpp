#include "MovingWall.h"

using namespace NCL;
using namespace CSC8503;

MovingWall::MovingWall(GameObjectType t, string name) : StateGameObject(t, name) {
	
}


MovingWall::~MovingWall() {
	delete stateA;
	delete stateB;
	delete transitionA;
	delete transitionB;
}

void MovingWall::initState() {
	someData.physObject = this->GetPhysicsObject();
	startingPosition = this->GetTransform().GetLocalPosition();

	stateMachine = new StateMachine();
	AFunc = [](void* data) {
		MovingWallStateData* realData = (MovingWallStateData*)data;
		realData->physObject->SetLinearVelocity(Vector3(realData->axis.x * realData->speed, realData->axis.y * realData->speed, realData->axis.z * realData->speed));
	};

	BFunc = [](void* data) {
		MovingWallStateData* realData = (MovingWallStateData*)data;
		realData->physObject->SetLinearVelocity(Vector3(realData->axis.x * -realData->speed, realData->axis.y * -realData->speed, realData->axis.z * -realData->speed));
	};

	stateA = new GenericState(AFunc, (void*)&this->someData);
	stateB = new GenericState(BFunc, (void*)&this->someData);
	stateMachine->AddState(stateA);
	stateMachine->AddState(stateB);

	transitionA = new GenericTransition<float&, float>(
		GenericTransition<float&, float>::LessThanTransition, dist, -maxDistance, stateA, stateB);

	transitionB = new GenericTransition<float&, float>(
		GenericTransition<float&, float>::GreaterThanTransition, dist, maxDistance, stateB, stateA);

	stateMachine->AddTransition(transitionA);
	stateMachine->AddTransition(transitionB);
}
