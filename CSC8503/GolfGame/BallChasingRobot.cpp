#include "BallChasingRobot.h"

using namespace NCL;
using namespace CSC8503;

BallChasingRobot::BallChasingRobot(GameObjectType t, string name) : StateGameObject(t, name) {

}


BallChasingRobot::~BallChasingRobot() {

}

void BallChasingRobot::initState() {
	stateMachine = new StateMachine();

	searchForTarget = [](void* data) {
		BallChasingRobotStateData* realData = (BallChasingRobotStateData*)data;
		std::cout << "Searching for ball" << std::endl;
	};

	followPath = [](void* data) {
		BallChasingRobotStateData* realData = (BallChasingRobotStateData*)data;
		std::cout << "Following path" << std::endl;
	};

	idleState = new GenericState(searchForTarget, (void*)&this->someData);
	followPathState = new GenericState(followPath, (void*)&this->someData);
	stateMachine->AddState(idleState);
	stateMachine->AddState(followPathState);

	foundTarget = new GenericTransition<bool&, bool > (
		GenericTransition<bool&, bool>::EqualsTransition, canSeeTarget, true, idleState, followPathState);
	lostTarget = new GenericTransition<bool&, bool>(
		GenericTransition<bool&, bool>::EqualsTransition, canSeeTarget, false, followPathState, idleState);

	stateMachine->AddTransition(foundTarget);
	stateMachine->AddTransition(lostTarget);
}