#pragma once
#include "../CSC8503Common/GameObject.h"
#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/State.h"
#include "../CSC8503Common/StateTransition.h"

namespace NCL {
	namespace CSC8503 {
		class StateGameObject : public GameObject
		{
		public:
			StateGameObject(GameObjectType t = GameObjectType::MovingWorldObject, string name = "");
			~StateGameObject();

			virtual void OnCollisionBegin(GameObject* otherObject) {

			}

			virtual void OnCollisionEnd(GameObject* otherObject) {

			}

			virtual void updateState(float dt = 0.0f) {

			}

			virtual void initState() {

			}

		protected:
			StateMachine* stateMachine;
		};
	}
}