#pragma once
#include "StateGameObject.h"
#include "..\CSC8503Common\NavigationGrid.h"
#include "..\CSC8503Common\Debug.h"

namespace NCL {
	namespace CSC8503 {
		class BallChasingRobot : public StateGameObject
		{
		public:
			BallChasingRobot(GameObjectType t = GameObjectType::MovingWorldObject, string name = "");
			~BallChasingRobot();

			bool canSeeObject(GameObject* object, GameWorld* world) {
				Ray ray(this->GetTransform().GetLocalPosition(), (object->GetTransform().GetLocalPosition() - this->GetTransform().GetLocalPosition()).Normalised());
				RayCollision closestCollision;
				if (world->Raycast(ray, closestCollision, true)) {
					if (object == (GameObject*)closestCollision.node) {
						return true;
					}
					return false;
				}
			}

			void setEyePos(Vector3 pos) {
				eyePos = pos;
			}

			const Vector3 getEyePos() const {
				return eyePos;
			}

			void drawPath() {
				for (int i = 1; i < pathNodes.size(); ++i) {
					Vector3 a = pathNodes[i - 1];
					Vector3 b = pathNodes[i];

					Debug::DrawLine(a, b, Vector4(0, 1, 0, 1));
				}
			}

			vector<Vector3> getPathNodes() {
				return pathNodes;
			}

			void buildPath(NavigationPath& path) {
				pathNodes.clear();
				Vector3 pos;
				while (path.PopWaypoint(pos)) {
					pathNodes.push_back(pos);
				}
			}

			void moveAlongPath() {
				if (pathNodes.size() != 0) {
					while (true) {
						if (pathNodes.size() != 0) {
							Vector3 nextPos = pathNodes.at(0);
							Vector3 currentPos = this->GetTransform().GetLocalPosition();
							nextPos.y = currentPos.y;
							Vector3 diff = (nextPos - currentPos);

							if (diff.Length() < 50.0f) {
								pathNodes.erase(pathNodes.begin());
							}
							else {
								break;
							}
						}
						else {
							break;
						}
					}
					if (pathNodes.size() != 0) {
						Vector3 nextPos = pathNodes.at(0);
						Vector3 currentPos = this->GetTransform().GetLocalPosition();

						Vector3 diff = (nextPos - currentPos);
						diff.y = 0;
						this->physicsObject->AddForce(diff.Normalised() * 75);
					}
				}
			}

			virtual void initState();

			virtual void updateState(float dt) {
				canSeeTarget = canSeeObject(someData.target, someData.world);
				stateMachine->Update();
			}

			void setWorld(GameWorld* w) {
				someData.world = w;
			}

			void setTarget(GameObject* target) {
				someData.target = target;
			}

			struct BallChasingRobotStateData {
				GameObject* target;
				GameWorld* world;
			};

		protected:
			Vector3 eyePos;
			vector<Vector3> pathNodes;

			StateFunc searchForTarget;
			StateFunc followPath;
			GenericState* idleState;
			GenericState* followPathState;
			GenericTransition<bool&, bool>* foundTarget;
			GenericTransition<bool&, bool>* lostTarget;

			BallChasingRobotStateData someData;

			bool canSeeTarget;
		};
	}
}