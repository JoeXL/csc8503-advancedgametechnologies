#pragma once

#include "GolfGameRenderer.h"
#include "../CSC8503Common/PhysicsSystem.h"
#include "GolfBall.h"
#include "MovingWall.h"
#include "BallChasingRobot.h"
#include "..\CSC8503Common\NavigationGrid.h"
#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"
#include <thread>

namespace NCL {
	namespace CSC8503 {
		class GolfGame {
		public:
			GolfGame();
			~GolfGame();

			virtual void UpdateGame(float dt);

		protected:
			void InitialiseAssets();
			
			void InitCamera();
			void resetCameraPosition();
			void moveCameraToBall();
			void UpdateKeys();
			void InitWorld(int i);

			void drawMenu();
			void InitMenuLevel();
			void InitTestLevel();
			void InitLevelOne();
			void InitLevelTwo();
			void InitLevelThree();
			void InitLevelFour();
			void InitLevelFive();

			void calculateShotVector();

			GameObject* AddFloorToWorld(const Vector3& position, string name = "");
			GameObject* AddWallToWorld(const Vector3& position, Vector3 dimensions, string name = "");
			GameObject* AddSphereToWorld(const Vector3& position, float radius, float inverseMass = 10.0f, string name = "");
			GameObject* AddCubeToWorld(const Vector3& position, Vector3 dimensions, float inverseMass = 10.0f, string name = "");
			GameObject* AddCubeOBBToWorld(const Vector3& position, Vector3 dimensions, float inverseMass = 10.0f, string name = "");
			GameObject* AddGolfBallToWorld(const Vector3& position, float radius, float inverseMass = 10.0f, string name = "");
			GameObject* AddGolfHoleToWorld(const Vector3& position, Vector3 dimensions, string name = "");
			GameObject* AddMovingWallToWorld(const Vector3& position, Vector3 dimensions, float maxDistance = 200.0f, float speed = 600.0f, Vector3 movementAxis = Vector3(1,0,0), string name = "");
			GameObject* AddBallChasingRobotToWorld(const Vector3& position, Vector3 dimensions, float inverseMass = 10.0f, string name = "");
			GameObject* AddOBBWallToWorld(const Vector3& position, Vector3 dimensions, Quaternion rotation, string name = "");

			void toggleCamFollowBall() { camFollowBall = !camFollowBall; }

			GolfGameRenderer* renderer;
			PhysicsSystem* physics;
			GameWorld* world;
			Vector3 defaultCameraPos = Vector3(0, 650, 1000);


			bool useGravity;
			bool inSelectionMode;
			bool drawingBallShotVector;
			bool drawNavGrid;
			bool drawNavPath;
			Vector3 ballShotVector;
			int currentLevel;
			bool onMainMenu;
			float forceMagnitude;
			bool camFollowBall;

			GameObject* selectionObject = nullptr;
			GolfBall* golfBall = nullptr;
			MovingWall* movingWall = nullptr;
			BallChasingRobot* robot = nullptr;
			NavigationGrid* currentLevelGrid = nullptr;

			vector<StateGameObject*> stateObjects;

			OGLMesh* cubeMesh = nullptr;
			OGLMesh* sphereMesh = nullptr;
			OGLTexture* basicTex = nullptr;
			OGLTexture* wallTex = nullptr;
			OGLTexture* ballTex = nullptr;
			OGLTexture* floorTex = nullptr;
			OGLTexture* robotTex = nullptr;
			OGLTexture* holeTex = nullptr;
			OGLShader* basicShader = nullptr;

			int levelCount = 5;

			void InitNetworking();

			bool InitServer();

			void InitClient();
			
			class HighScoreReceiver : public PacketReceiver {
			public:
				HighScoreReceiver(string name, vector<int>* scores) {
					this->name = name;
					this->scores = scores;
				}

				void ReceivePacket(int type, GamePacket* payload, int source) {
					if (type == BasicNetworkMessages::String_Message) {
						StringPacket* realPacket = (StringPacket*)payload;
						string msg = realPacket->GetStringFromData();
					}
					else if (type == BasicNetworkMessages::HighScore_Message) {
						HighScorePacket* realPacket = (HighScorePacket*)payload;
						if (scores->at(realPacket->levelNum) == -1 || (realPacket->score < scores->at(realPacket->levelNum) && realPacket->score > 0)) {
							scores->at(realPacket->levelNum) = realPacket->score;
						}
					}
				}

			protected:
				string name;
				vector<int>* scores;
			};

			vector<int> highScores;

			int port;
			GameServer* server;
			GameClient* client;
			HighScoreReceiver* serverReceiver;
			HighScoreReceiver* clientReceiver;
		};
	}
}