#pragma once
#include "../CSC8503Common/GameObject.h"

namespace NCL {
	namespace CSC8503 {
		class GolfBall : public GameObject {
		public:
			GolfBall(GameObjectType t = GameObjectType::GameObject, string name = "");
			~GolfBall();


			virtual void OnCollisionBegin(GameObject* otherObject) {
				if (otherObject->GetName() == "Hole") {
					collidedWithHole = true;
				}
				else if (otherObject->GetName() == "Robot") {
					collidedWithRobot = true;
				}
			}

			virtual void OnCollisionEnd(GameObject* otherObject) {

			}

			const bool hasCollidedWithHole() const { return collidedWithHole; }
			void setCollidedWithHole(bool b) { collidedWithHole = b; }

			const bool hasCollidedWithRobot() const { return collidedWithRobot; }
			void setCollidedWithRobot(bool b) { collidedWithRobot = b; }

			void incrementShotCount() { shotCount++; }
			const int getShotCount() const { return shotCount; }
			void resetShotCount() { shotCount = 0; }

			void setMaxShotForce(float f) { maxShotForce = f; }
			const float getMaxShotForce() const { return maxShotForce; }

		protected:
			bool collidedWithHole;
			bool collidedWithRobot;
			int shotCount;
			float maxShotForce;
		};
	}
}