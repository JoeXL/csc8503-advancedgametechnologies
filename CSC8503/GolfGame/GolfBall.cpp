#include "GolfBall.h"

using namespace NCL;
using namespace CSC8503;

GolfBall::GolfBall(GameObjectType t, string objectName) : GameObject(t, objectName) {
	collidedWithHole = false;
	collidedWithRobot = false;
	shotCount = 0;
	maxShotForce = 200.0f;
}


GolfBall::~GolfBall() {
	delete boundingVolume;
	delete physicsObject;
	delete renderObject;
	delete networkObject;
}
