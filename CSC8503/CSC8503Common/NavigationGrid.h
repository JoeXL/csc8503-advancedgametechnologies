#pragma once
#include "NavigationMap.h"
#include "..\..\Common\Vector2.h"
#include <string>
#include "GameWorld.h"
namespace NCL {
	namespace CSC8503 {
		enum NodeType {
			GROUND,
			WALL
		};

		struct GridNode {
			GridNode* parent;

			GridNode* connected[4];
			int		  costs[4];

			Vector3		position;

			float f;
			float g;

			int type;

			GridNode() {
				for (int i = 0; i < 4; ++i) {
					connected[i] = nullptr;
					costs[i] = 0;
				}
				f = 0;
				g = 0;
				type = 0;
				parent = nullptr;
			}
			~GridNode() {	}
		};

		class NavigationGrid : public NavigationMap	{
		public:
			NavigationGrid();
			NavigationGrid(int nodeSize, int width, int height, Vector3 origin, const GameWorld& world);
			NavigationGrid(const std::string&filename);
			~NavigationGrid();

			void drawGrid();

			bool FindPath(const Vector3& from, const Vector3& to, NavigationPath& outPath) override;
				
		protected:
			bool		NodeInList(GridNode* n, std::vector<GridNode*>& list) const;
			GridNode*	RemoveBestNode(std::vector<GridNode*>& list) const;
			float		Heuristic(GridNode* hNode, GridNode* endNode) const;
			int nodeSize;
			int gridWidth;
			int gridHeight;

			Vector3 gridOrigin = Vector3(0,0,0);

			GridNode* allNodes;
		};
	}
}

